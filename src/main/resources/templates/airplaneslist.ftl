<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List of Airplanes</title>
</head>
<body>
<h1>Aiplaines list</h1>
<table border="2" width="50%" cellpadding="2">
    <tr>
        <th>id</th>
        <th>name</th>
        <th>number of flights</th>
        <th>flights distance</th>
        <th>capacity</th>
        <th>carrying capacity</th>
        <th>Fuel consumption</th>
        <th>aircompany</th>
        <th>Delete</th>
        <th>Edit</th>

    </tr>
    <#list airplanes as air>
        <tr>
            <td>${air.id}</td>
            <td>${air.name}</td>
            <td>${air.numberOfFlights}</td>
            <td>${air.flightsDistance} km</td>
            <td>${air.capacity} L</td>
            <td>${air.carryingCapacityKg} kg</td>
            <td>${air.fuelСonsumption} L</td>
            <td>${air.aircompany.name}</td>
            <td><a href="/airplanes/delete/${air.id}">Delete</a></td>
            <td><a href="/airplanes/edit/${air.id}">Edit</a></td>
        </tr>

    </#list>
</table>
<a href="/">Back</a>

</body>
</html>


