<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List of Aircompanies</title>
</head>
<body>
<h1>Aircompanies List</h1>
<a href="/aircompanies/addcompany">Add new aircompany</a>
<a href="/airplanes/">Airplanes list</a>
<a href="/aircompanies/sort" style="color: red">Sort by flight distance</a>
<form action="/logout" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="submit" value="Sign Out"/>
</form>
<br>
<br>
<table border="2" width="50%" cellpadding="2">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Type of company</th>
        <th>Airplanes</th>
        <th>Edit</th>
        <th>Delete</th>

    </tr>
    <#list aircompanies as aircom>
    <tr>
        <td><a href="/aircompanies/${aircom.company_id}">${aircom.company_id}</a></td>
        <td>${aircom.name}</td>
        <td>${aircom.typeOfCompany}</td>
        <td>
            <#if aircom.airplanes[0]?has_content>

                <table>
                    <tr>
                        <th>id</th>
                        <th>&emsp;&emsp;&emsp;</th>
                        <th>name</th>
                    </tr>
                    <#list aircom.airplanes as air>
                        <tr>
                        <td><a href="/airplanes/${air.id}">${air.id}</a></td>
                        <td>&emsp;&emsp;&emsp;</td>
                        <td>${air.name}</td>
                    </#list></table> </#if>
        </td>
        <td><a href="/aircompanies/delete/${aircom.company_id}">Delete</a></td>
        <td><a href="/aircompanies/edit/${aircom.company_id}">Edit</a></td>
        </#list>
    </tr>

</table>


</body>
</html>


