<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create user page</title>
</head>
<body>

<form name="aircompany" action="/aircompanies/addcompany" method="post">
    <p>Name</p>
    <input title="Name" type="text" name="name">
    <p>Type of company</p>
    <input title="Type of company" type="text" name="typeOfCompany">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="submit" value="add">
</form>
<a href="/">Back</a>
</body>
</html>