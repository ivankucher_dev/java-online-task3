<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aircompany ${aircom.name}</title>
</head>
<body>
<h1>Aircompany info</h1>
<table>
    <tr>
        <td>ID :</td>
        <td>${aircom.company_id}</td>
    </tr>
    <tr>
        <td>Name :</td>
        <td>${aircom.name}</td>
    </tr>
    <tr>
        <td>Type of company :</td>
        <td>${aircom.typeOfCompany}</td>
    </tr>
    <tr>
</table>

<br>
<a href="/aircompanies/${aircom.company_id}/calculate/" style="color: red">Calculate total capacity and carrying
    capacity</a>
<br>
<#if aircom.airplanes[0]?has_content>
    <h1>List of airplanes of ${aircom.name} company</h1>
    <table border="2" width="50%" cellpadding="2">
        <tr>
            <th>id</th>
            <th>name</th>
            <th>numberOfFlights</th>
            <th>flightsDistance</th>
            <th>capacity</th>
            <th>carryingCapacityKg</th>
            <th>Fuel consumption</th>
            <th>aircompany</th>
            <th>Delete</th>
            <th>Edit</th>

        </tr>
        <#list aircom.airplanes as air>
            <tr>
                <td><a href="/airplanes/${air.id}">${air.id}</a></td>
                <td>${air.name}</td>
                <td>${air.numberOfFlights}</td>
                <td>${air.flightsDistance} km</td>
                <td>${air.capacity} L</td>
                <td>${air.carryingCapacityKg} kg</td>
                <td>${air.fuelСonsumption} L</td>
                <td>${air.aircompany.name}</td>
                <td><a href="/airplanes/delete/${air.id}">Delete</a></td>
                <td><a href="/airplanes/edit/${air.id}">Edit</a></td>
            </tr>

        </#list>
    </table>
</#if>
<a href="/airplanes/addairplane/${aircom.company_id}" style="color: red">Add new airplane</a>
<br>
<a href="/">Back</a>
<br>
<h1>Find airplane by fuel range</h1>
<form name="range" action="/aircompanies/${aircom.company_id}/sortByFuelRange" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <p>Lower range</p>
    <input title="Lower Range" type="text" name="lowerRange">
    <p>Upper range</p>
    <input title="Upper range" type="text" name="upperRange">
    <input type="submit" value="find">
</form>

</body>
</html>





