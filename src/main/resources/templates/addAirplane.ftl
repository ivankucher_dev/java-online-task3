<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add airplane</title>
</head>
<body>

<form name="airplane" action="/airplanes/addairplane/${aircompany.company_id}" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <p>Name</p>
    <input title="Name" type="text" name="name">
    <p>Number of flights</p>
    <input title="Number of flights" type="text" name="numberOfFlights">
    <p>Flights distance</p>
    <input title="Flights distance" type="text" name="flightsDistance">
    <p>Capacity</p>
    <input title="Capacity" type="text" name="capacity">
    <p>Carrying capacity</p>
    <input title="Carrying capacity" type="text" name="carryingCapacityKg">
    <p>Fuel consumption</p>
    <input title="fuelСonsumption" type="text" name="fuelСonsumption">
    <p>Aircompany ${aircompany.name}</p>
    <input title="aircompany" type="text" value="${aircompany.company_id}" name="aircompany" readonly>
    <input type="submit" value="add">
</form>

</body>
</html>

