<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User</title>
</head>
<body>
<h1>Airplane info</h1>
<table>
    <tr>
        <td>ID :</td>
        <td>${airplane.id}</td>
    </tr>
    <tr>
        <td>Name :</td>
        <td>${airplane.name}</td>
    </tr>
    <tr>
        <td>Number of flights :</td>
        <td>${airplane.numberOfFlights}</td>
    </tr>
    <tr>
        <td>Flights Distance :</td>
        <td>${airplane.flightsDistance} km</td>
    </tr>
    <tr>
        <td>Capacity :</td>
        <td>${airplane.capacity} L</td>
    </tr>
    <tr>
        <td>Carrying capacity :</td>
        <td>${airplane.carryingCapacityKg} kg</td>
    </tr>
    <tr>
        <td> Fuel consumption :</td>
        <td>${airplane. fuelСonsumption} L</td>
    </tr>
    <tr>
        <td>Aircompany :</td>
        <td>${airplane.aircompany.name}</td>
    </tr>

</table>

<br>


<br>
<a href="/">Back</a>
</body>
</html>





