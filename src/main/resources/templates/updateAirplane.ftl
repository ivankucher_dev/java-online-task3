<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit airplane</title>
</head>
<body>


<form name="airplane" action="/airplanes/edit/${airplane.id}" method="post">
    <p>ID</p>
    <input title="Id" type="text" value="${airplane.id}" name="id">
    <p>Name</p>
    <input title="Name" type="text" value="${airplane.name}" name="name">
    <p>Number of flights</p>
    <input title="Number of flights" type="text" value="${airplane.numberOfFlights}" name="numberOfFlights">
    <p>Flights distance</p>
    <input title="Flights distance" type="text" value="${airplane.flightsDistance}" name="flightsDistance">
    <p>Capacity</p>
    <input title="Capacity" type="text" value="${airplane.capacity}" name="capacity">
    <p>Carrying capacity</p>
    <input title="Carrying capacity" type="text" value="${airplane.carryingCapacityKg}" name="carryingCapacityKg">
    <p>Fuel consumption</p>
    <input title="  fuelСonsumption" type="text" value="${airplane.fuelСonsumption}" name="fuelСonsumption">
    <p>Aircompany now - ${aircompany.name}</p>
    <input list="aircompanies" id="selected" name="aircompany" value="${aircompany.company_id}">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <datalist id="aircompanies">
        <#list aircompanies as aircom>

            <option value="${aircom.company_id}" name="aircompany">${aircom.name}</option>

        </#list>
    </datalist>
    <input type="submit" value="Edit">
</form>


</body>
</html>