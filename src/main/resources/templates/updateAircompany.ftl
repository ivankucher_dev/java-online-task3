<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit aircompany</title>
</head>
<body>


<form name="aircompany" action="/aircompanies/edit/${aircompany.company_id}" method="post">
    <p>ID</p>
    <input title="Id" type="text" value="${aircompany.company_id}" name="company_id" readonly>
    <p>Name</p>
    <input title="Name" type="text" value="${aircompany.name}" name="name">
    <p>Type of company</p>
    <input title="typeOfCompany" type="text" value="${aircompany.typeOfCompany}" name="typeOfCompany">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="submit" value="Edit">
</form>


</body>
</html>