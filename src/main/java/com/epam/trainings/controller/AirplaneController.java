package com.epam.trainings.controller;

import com.epam.trainings.entity.Aircompany;
import com.epam.trainings.entity.Airplane;
import com.epam.trainings.repository.AircompanyRepo;
import com.epam.trainings.repository.AirplaneRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("/airplanes")
public class AirplaneController {


    private final AircompanyRepo aircompanyRepo;
    private final AirplaneRepo airplaneRepo;

    public AirplaneController(AircompanyRepo aircompanyRepo, AirplaneRepo airplaneRepo) {
        this.aircompanyRepo = aircompanyRepo;
        this.airplaneRepo = airplaneRepo;
    }

    @GetMapping("")
    public String getAllAirplanes(Model model) {
        model.addAttribute("airplanes", airplaneRepo.findAll());
        return "airplaneslist";
    }


    @GetMapping("/{id}")
    public String getAirplaneById(@PathVariable("id") int id, Model model) {
        Airplane airplane = airplaneRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid plane Id:" + id));
        model.addAttribute("airplane", airplane);
        return "showairplane";
    }

    @GetMapping("/addairplane/{company_id}")
    public String addAirplane(@PathVariable("company_id") int id, Model model) {
        Aircompany aircompany = aircompanyRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid aircompany Id:" + id));
        model.addAttribute("aircompany", aircompany);
        return "addAirplane";
    }


    @PostMapping("/addairplane/{company_id}")
    public String addAirplane(@ModelAttribute("airplane") Airplane airplane) {
        airplaneRepo.save(airplane);
        return "redirect:/aircompanies/{company_id}";
    }


    @GetMapping("/delete/{id}")
    public String deleteAirplane(@PathVariable("id") int id) {
        airplaneRepo.deleteById(id);
        return "redirect:/airplanes/";
    }


    @GetMapping("/edit/{id}")
    public String editAirplane(@PathVariable("id") int id, Model model) {
        Airplane airplane = airplaneRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid plane Id:" + id));
        Map<String, Object> attributes = new HashMap<>();
        Aircompany aircompany = aircompanyRepo.findById(airplane.getAircompany().getCompany_id())
                .orElseThrow(() -> new IllegalArgumentException("Invalid company Id:" + id));
        List<Aircompany> aircompanies = new ArrayList<>();
        aircompanyRepo.findAll().forEach(aircompanies::add);
        attributes.put("aircompanies", aircompanies);
        attributes.put("aircompany", aircompany);
        attributes.put("airplane", airplane);
        model.addAllAttributes(attributes);
        return "updateAirplane";
    }


    @PostMapping("/edit/{id}")
    public String updateAirplane(@PathVariable("id") int id, @ModelAttribute("airplane") Airplane airplane) {
        airplane.setId(id);
        airplaneRepo.save(airplane);
        return "redirect:/airplanes/" + id;
    }
}
