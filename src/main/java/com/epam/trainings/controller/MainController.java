package com.epam.trainings.controller;


import com.epam.trainings.entity.Aircompany;
import com.epam.trainings.entity.Airplane;
import com.epam.trainings.repository.AircompanyRepo;
import com.epam.trainings.repository.AirplaneRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class MainController {


    private final AircompanyRepo aircompanyRepo;
    private final AirplaneRepo airplaneRepo;

    public MainController(AircompanyRepo aircompanyRepo, AirplaneRepo airplaneRepo) {
        this.aircompanyRepo = aircompanyRepo;
        this.airplaneRepo = airplaneRepo;
    }



    @GetMapping("/")
    public String getAllAircompanies(Model model) {
        model.addAttribute("aircompanies", aircompanyRepo.findAll());
        return "aircompanyList";
    }


}
