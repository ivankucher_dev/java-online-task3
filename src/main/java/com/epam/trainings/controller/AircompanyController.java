package com.epam.trainings.controller;

import com.epam.trainings.entity.Aircompany;
import com.epam.trainings.entity.Airplane;
import com.epam.trainings.exception.BadFuelRangeException;
import com.epam.trainings.repository.AircompanyRepo;
import com.epam.trainings.repository.AirplaneRepo;
import com.epam.trainings.utils.AirplaneFuelRange;
import com.epam.trainings.utils.CustomComparator;
import jdk.nashorn.internal.ir.LiteralNode;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("/aircompanies")
public class AircompanyController {


    private final AircompanyRepo aircompanyRepo;
    private final AirplaneRepo airplaneRepo;

    public AircompanyController(AircompanyRepo aircompanyRepo, AirplaneRepo airplaneRepo) {
        this.aircompanyRepo = aircompanyRepo;
        this.airplaneRepo = airplaneRepo;
    }


    @GetMapping("/addcompany")
    public String addAircompany() {
        return "createAircompany";
    }


    @PostMapping("/addcompany")
    public String addAircompany(@ModelAttribute("aircompany") Aircompany aircompany) {
        aircompanyRepo.save(aircompany);
        return "redirect:/";
    }

    @GetMapping("/{id}")
    public String getAircompanyById(@PathVariable("id") int id, Model model) {
        Aircompany aircompany = aircompanyRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid company Id:" + id));
        model.addAttribute("aircom", aircompany);
        return "showaircompany";
    }

    @GetMapping("/delete/{id}")
    public String deleteAircompanies(@PathVariable("id") int id) {
        Aircompany aircompany = aircompanyRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid company Id:" + id));
        List<Airplane> airplanes = aircompany.getAirplanes();
        List<Integer> listOfAirplanes_id = new ArrayList<>();

        for (Airplane airplane : airplanes) {
            listOfAirplanes_id.add(airplane.getId());
        }
        aircompanyRepo.delete(aircompany);

        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String editAircompany(@PathVariable("id") int id, Model model) {
        Aircompany aircompany = aircompanyRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid plane Id:" + id));
        model.addAttribute("aircompany", aircompany);
        return "updateAircompany";
    }


    @PostMapping("/edit/{id}")
    public String editAircompany(@PathVariable("id") int id, @ModelAttribute("aircompany") Aircompany aircompany) {
        aircompanyRepo.save(aircompany);
        return "redirect:/aircompanies/" + id;
    }


    @GetMapping("/sort")
    public String sortByFlightDistance(Model model) {
        Iterable<Aircompany> aircom = aircompanyRepo.findAll();
        ArrayList<Aircompany> aircompanies = new ArrayList<>();
        aircom.forEach(aircompanies::add);
        Collections.sort(aircompanies, Comparator.comparing((Aircompany aircompany) -> aircompany.getAllFlightDistance()));
        model.addAttribute("aircompanies", aircompanies);

        return "aircompanyList";
    }


    @GetMapping("/{id}/calculate")
    public String calculateTotalValues(@PathVariable("id") int id, Model model) {
        Aircompany aircompany = aircompanyRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid plane Id:" + id));
        List<Double> totalValues = aircompany.getTotalCapacityAndCarryingCapacity();
        Map<String, Double> attributes = new HashMap<>();
        attributes.put("capacity", totalValues.get(0));
        attributes.put("carryingCapacity", totalValues.get(1));
        model.addAllAttributes(attributes);
        return "aircompanytotalreport";
    }


    @RequestMapping(value = "/{id}/sortByFuelRange", method = RequestMethod.POST)
    public String getAirplaneFromFuelRange(@PathVariable("id") int id, @RequestParam("lowerRange") double lowerRange, @RequestParam("upperRange") double upperRange, Model model) throws BadFuelRangeException {
        Aircompany aircompany = aircompanyRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid company Id:" + id));
        List<Airplane> airplanes = new ArrayList<>();
        if(upperRange==lowerRange){
            throw new BadFuelRangeException("same values");
        }
        try(AirplaneFuelRange airplaneFuelRange = new AirplaneFuelRange()) {
            System.out.println("Reading data "+ lowerRange +" and "+ upperRange);
            airplaneFuelRange.setLowerRange(lowerRange);
            airplaneFuelRange.setUpperRange(upperRange);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Airplane airplane : aircompany.getAirplanes()) {
            if (airplane.getFuelСonsumption() <= upperRange && airplane.getFuelСonsumption() >= lowerRange) {
                airplanes.add(airplane);

            }
            model.addAttribute("airplanes", airplanes);
        }
        return "airplaneslist";
    }
}
