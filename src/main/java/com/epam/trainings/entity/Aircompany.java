package com.epam.trainings.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.*;


@Entity
public class Aircompany {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int company_id;

    private String name;
    private String typeOfCompany;

    @OneToMany(mappedBy = "aircompany", cascade = CascadeType.ALL)
    private List<Airplane> airplanes;

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeOfCompany() {
        return typeOfCompany;
    }

    public void setTypeOfCompany(String typeOfCompany) {
        this.typeOfCompany = typeOfCompany;
    }

    public List<Airplane> getAirplanes() {
        return airplanes;
    }

    public void setAirplanes(List<Airplane> airplanes) {
        this.airplanes = airplanes;
    }


    public double getAllFlightDistance() {
        int flightDistance = 0;
        for (Airplane airplane : airplanes) {
            flightDistance += airplane.getFlightsDistance();
        }
        return flightDistance;
    }

    public List<Double> getTotalCapacityAndCarryingCapacity() {
        double counterCapacity = 0;
        double counterCarrying = 0;
        List<Double> totalValues = new ArrayList<>();
        for (Airplane airplane : airplanes) {
            counterCapacity += airplane.getCapacity();
            counterCarrying += airplane.getCarryingCapacityKg();
        }
        totalValues.add(counterCapacity);
        totalValues.add(counterCarrying);
        return totalValues;
    }


}
