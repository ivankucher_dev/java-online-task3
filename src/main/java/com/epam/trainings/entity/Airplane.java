package com.epam.trainings.entity;

import javax.persistence.*;


@Entity
@Table(name = "Airplane")
public class Airplane {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private double numberOfFlights;
    private double flightsDistance;
    private double capacity;
    private double carryingCapacityKg;
    private double fuelСonsumption;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Aircompany aircompany;

    public Aircompany getAircompany() {
        return aircompany;
    }

    public void setAircompany(Aircompany aircompany) {
        this.aircompany = aircompany;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNumberOfFlights() {
        return numberOfFlights;
    }

    public void setNumberOfFlights(double numberOfFlights) {
        this.numberOfFlights = numberOfFlights;
    }

    public double getFlightsDistance() {
        return flightsDistance;
    }

    public void setFlightsDistance(double flightsDistance) {
        this.flightsDistance = flightsDistance;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public double getCarryingCapacityKg() {
        return carryingCapacityKg;
    }

    public void setCarryingCapacityKg(double carryingCapacityKg) {
        this.carryingCapacityKg = carryingCapacityKg;
    }

    public double getFuelСonsumption() {
        return fuelСonsumption;
    }

    public void setFuelСonsumption(double fuelСonsumption) {
        this.fuelСonsumption = fuelСonsumption;
    }
}
