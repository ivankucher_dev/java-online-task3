package com.epam.trainings.repository;


import com.epam.trainings.entity.Aircompany;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.epam.trainings.entity.Airplane;
import org.springframework.transaction.annotation.Transactional;

@Repository
@EntityScan(basePackages = {"com.epam.trainings.com.epam.trainings.entity", "com.epam.trainings.repository"})
public interface AirplaneRepo extends CrudRepository<Airplane, Integer> {

    @Transactional
    @Query(value = "UPDATE Airplane SET name=:newName , numberOfFlights=:newNumberOfFlights," +
            " flightsDistance=:newFlightsDistance, capacity=:newCapacity," +
            "  carryingCapacityKg=:newCarryingCapacityKg,aircompany=:newAircompany where id =:airplane_id",
            nativeQuery = true)
    Airplane updateAirplane(@Param("newName") String name, @Param("newNumberOfFlights") double numberOfFlights,
                            @Param("newFlightsDistance") double flightsDistance, @Param("newCapacity") double capacity,
                            @Param("newCarryingCapacityKg") double carryingCapacityKg, @Param("newAircompany") int company_id,
                            @Param("airplane_id") int id);

}


