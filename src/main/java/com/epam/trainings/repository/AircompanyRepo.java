package com.epam.trainings.repository;

import com.epam.trainings.entity.Aircompany;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@EntityScan(basePackages = {"com.epam.trainings.com.epam.trainings.entity", "com.epam.trainings.repository"})
public interface AircompanyRepo extends CrudRepository<Aircompany, Integer> {

}
