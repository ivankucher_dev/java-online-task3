package com.epam.trainings.exception;

public class BadFuelRangeException extends Exception {

    public BadFuelRangeException(String message){
        super(message);
    }

}
