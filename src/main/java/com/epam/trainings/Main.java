package com.epam.trainings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableJpaRepositories("com.epam.trainings*")
@ComponentScan(basePackages = {"com.epam.trainings.entity", "com.epam.trainings.repository", "com.epam.trainings.controller", "com.epam.trainings.utils", "com.epam.trainings.config"})
@EntityScan("com.epam.trainings.entity")
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
