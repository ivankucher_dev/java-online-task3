package com.epam.trainings.utils;

import javax.persistence.Entity;

@Entity
public class AirplaneFuelRange implements AutoCloseable {

    public double lowerRange;
    public double upperRange;

    public double getLowerRange() {
        return lowerRange;
    }

    public void setLowerRange(double lowerRange) {
        this.lowerRange = lowerRange;
    }

    public void setUpperRange(double upperRange) {
        this.upperRange = upperRange;
    }

    public double getUpperRange() {
        return upperRange;
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing resource");
    }
}
