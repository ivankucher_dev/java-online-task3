package com.epam.trainings.utils;

import com.epam.trainings.entity.Aircompany;
import com.epam.trainings.entity.Airplane;

import java.util.Comparator;

public class CustomComparator implements Comparator<Aircompany> {
    @Override
    public int compare(Aircompany o1, Aircompany o2) {
     return (int) (o2.getAllFlightDistance() - o1.getAllFlightDistance());
    }

}
